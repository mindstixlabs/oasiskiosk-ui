/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.OasisKioskUi.screens;



import org.openqa.selenium.Keys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.vmware.OasisKioskUi.setup.TestSuiteSetup;
import com.vmware.OasisKioskUi.utils.Constants;
import com.vmware.OasisKioskUi.utils.ExtentTestCase;


/**
 * Contains methods for dashbaord.
 * 
 * @author Mindstix
 *
 */
public class Dashboard extends BaseScreen {
  private static final Logger LOGGER = LoggerFactory.getLogger(Dashboard.class);
  private Login login = new Login();

  /**
   * Method for login as a end user.
   */
  public void loginForEndUser() {
    ExtentTestCase.getTest().info("Check in with end user name");
    getElement("user_name_acc_id").sendKeys(decode(Constants.USER_NAME));
    TestSuiteSetup.driver.getKeyboard().sendKeys(Keys.ENTER);
    waitUntilVisiblityWithTime("select_reason_label_xpath");
    ExtentTestCase.getTest().info("User is on dashboard");
  }

  /**
   * Method to cancel button and check in button functionality.
   */
  public void verifyCancelAndCheckinButtonFuncationality() {
    login.loginTestForAdmin();
    loginForEndUser();
    if (getElement("check_in_acc_id").isEnabled()) {
      ExtentTestCase.getTest().info("User is on dashboard");
      getElement("check_in_acc_id").click();
    } else {
      LOGGER.info("Check in button is disabled");
      ExtentTestCase.getTest().info("Check in button is disabled");
      getElement("cancel_acc_id").click();
    }
  }    
}
