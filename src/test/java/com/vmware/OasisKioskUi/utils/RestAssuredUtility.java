/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.OasisKioskUi.utils;

import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;

/**
 * Set of helper utilities for invocation of REST APIs.
 * 
 * @author Mindstix
 */
public final class RestAssuredUtility {

  // Global Setup Variables
  private static String restRequestPath; // REST request path

  /**
   * Sets Base ExtendedURI Before starting the test, we should set the
   * RestAssured.baseURI
   * 
   * @param baseUri
   */
  public static void setBaseUri(String baseUri) {
    RestAssured.baseURI = baseUri;
  }

  public static String getBaseURI() {
    return RestAssured.baseURI;
  }

  /**
   * Sets base path Before starting the test, we should set the
   * RestAssured.basePath
   * 
   * @param basePathTerm
   */
  public static void setBasePath(String basePathTerm) {
    RestAssured.basePath = basePathTerm;
  }

  public static String getBasePath() {
    return RestAssured.basePath;
  }

  /**
   * Set port number for the URL invocation.
   */
  public static void setPortNo(int port) {
    RestAssured.port = port;
  }

  /**
   * Reset Base ExtendedURI (after test) After the test, we should reset the
   * RestAssured.baseURI
   */
  public static void resetBaseUri() {
    RestAssured.baseURI = null;
  }

  /**
   * Reset base path (after test) After the test, we should reset the
   * RestAssured.basePath
   */
  public static void resetBasePath() {
    RestAssured.basePath = null;
  }

  /**
   * Sets ContentType We should set content type as JSON or XML before starting
   * the test.
   */
  public static void setContentType(ContentType type) {
    given().contentType(type);
  }

  /**
   * Construct a ExtendedURI path for a search query based on the input
   * parameters.
   * 
   * @param searchTerm
   * @param jsonPathTerm
   * @param param
   * @param paramValue
   * 
   */
  public static void createSearchQueryPath(String searchTerm, String jsonPathTerm, String param, String paramValue) {
    restRequestPath = searchTerm + "/" + jsonPathTerm + "?" + param + "=" + paramValue;
  }

  /**
   * Returns response We send "path" as a parameter to the Rest Assured'a "get"
   * method. "get" method returns response of API
   * 
   * @return response
   */
  public static Response getResponse() {
    return get(restRequestPath);
  }

  /**
   * Returns JsonPath object First convert the API's response to String type with
   * "asString()" method. Then, send this String formatted JSON response to the
   * JsonPath class and return the JsonPath.
   * 
   * @param res
   * @return JSON path
   */
  public static JsonPath getJsonPath(Response res) {
    String json = res.asString();
    return new JsonPath(json);
  }

  /**
   * Method to decode the base64 string.
   * 
   * @param encodedString
   * @return decoded String
   */
  public static String decode(String encodedString) {
    return StringUtils.newStringUtf8(Base64.decodeBase64(encodedString));
  }
}
