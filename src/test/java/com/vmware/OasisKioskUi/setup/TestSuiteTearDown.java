/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.OasisKioskUi.setup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Performs the tear down action for the driver.
 * 
 * @author mindstix
 *
 */
public class TestSuiteTearDown {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestSuiteTearDown.class);

    /**
     * Close the the driver after Execution.
     */
    public static void teardown() {
        TestSuiteSetup.driver.quit();
        LOGGER.info("Closed the App");
    }

}