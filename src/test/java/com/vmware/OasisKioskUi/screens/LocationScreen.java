/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */
package com.vmware.OasisKioskUi.screens;

import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.Keys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.vmware.OasisKioskUi.api.ServiceProvider;
import com.vmware.OasisKioskUi.setup.TestSuiteSetup;
import com.vmware.OasisKioskUi.utils.Constants;
import com.vmware.OasisKioskUi.utils.ExtentTestCase;

import io.appium.java_client.MobileElement;

/**
 * Class contains a method for location screen
 * 
 * @author Mindstix
 *
 */
public class LocationScreen extends BaseScreen {
  private static final Logger LOGGER = LoggerFactory.getLogger(LocationScreen.class);
  private ServiceProvider serviceProvider = new ServiceProvider();

  /**
   * Method to check location list.
   */
  public void listOfLocation() {
    waitUntilVisible("user_name_acc_id");
    loginForAdmin();
    JSONObject jsonObj = serviceProvider.getHelpDeskDetails();
    JSONArray jsonArray = (JSONArray) jsonObj.get("result");
    LOGGER.info("JsonArray size: {} ", jsonArray.length());
    List<MobileElement> locationList = (List<MobileElement>) TestSuiteSetup.driver
        .findElementsByClassName("XCUIElementTypeStaticText");
    LOGGER.info("Size : {} ", locationList.size());
    LOGGER.info("Locations are displayed");
    ExtentTestCase.getTest().info("Locations are displayed");
    for (int i=0; i< Constants.locations.length;i++) {
      String location = Constants.locations[i].toString();    
      String helpdeskItem = ((JSONObject) jsonArray.get(i)).get("helpdesk_name").toString();
      LOGGER.info("Actual (Ui) : {} Expected (Api) : {} ", location, helpdeskItem);
      ExtentTestCase.getTest().info("Actual :  " + location + " Expected :  " + helpdeskItem);    
    }
    swipeUp();
    getElement("done_acc_id").click();
  }

  /**
   * Method for login as a admin user.
   */
  public void loginForAdmin() {
    getElement("user_name_acc_id").sendKeys(decode(Constants.USER_NAME_ADMIN));
    TestSuiteSetup.driver.getKeyboard().sendKeys(Keys.ENTER);
    isElementPresent("Change_location_label_acc_id");
  }
}
