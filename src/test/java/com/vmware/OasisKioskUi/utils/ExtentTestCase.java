/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.OasisKioskUi.utils;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

/**
 * Contains objects of extent report.
 * 
 * @author Mindstix
 */
public class ExtentTestCase {
    static ExtentTest Test;
    static ExtentReports Extent;

    public static ExtentTest getTest() {
        return Test;
    }

    public static void setTest(ExtentTest test) {
        Test = test;
    }

    public static ExtentReports getExtent() {
        return Extent;
    }

    public static void setEXTENT(ExtentReports extent) {
        Extent = extent;
    }

}
