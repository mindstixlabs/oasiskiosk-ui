/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.OasisKioskUi.test;

import java.io.IOException;
import java.net.MalformedURLException;
import org.testng.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.aventstack.extentreports.Status;
import com.vmware.OasisKioskUi.screens.BaseScreen;
import com.vmware.OasisKioskUi.screens.Dashboard;
import com.vmware.OasisKioskUi.screens.LocationScreen;
import com.vmware.OasisKioskUi.screens.Login;
import com.vmware.OasisKioskUi.setup.TestSuiteSetup;
import com.vmware.OasisKioskUi.setup.TestSuiteTearDown;
import com.vmware.OasisKioskUi.utils.ExtentManager;
import com.vmware.OasisKioskUi.utils.ExtentTestCase;
import com.vmware.OasisKioskUi.utils.ScreenshotUtility;

/**
 * This class contains methods for all the test cases.
 * 
 * @author Mindstix
 *
 */
public class TestScript {
  private static final Logger LOGGER = LoggerFactory.getLogger(TestScript.class);
  private BaseScreen objCommon = new BaseScreen();
  private ScreenshotUtility screenshotUtilObj = new ScreenshotUtility();
  private Login login = new Login();
  private LocationScreen locationScreen = new LocationScreen();
  private Dashboard dashboard = new Dashboard();

  /**
   * Setup method gets called first before executing any test.
   */
  @BeforeTest(groups = { "smoke", "regression" })
  public void setup() {
    LOGGER.info("In BeforeTest : Set up for Oasis Kiosk App view ");
    ExtentTestCase.setEXTENT(ExtentManager.getExtent());
  }

  /**
   * Verify Login Functionality for Admin User and setup for app location.
   */
  @Test(groups = { "smoke", "regression" }, priority = 1, enabled = true)
  public void verifyAppLoginForAdmin() throws InterruptedException {
    LOGGER.info("Executing 1st test case : Login Screen test.");
    ExtentTestCase.setTest(ExtentTestCase.getExtent().createTest("Application Login For Admin",
        "Test the application login module for admin user"));   
    ExtentTestCase.getTest().info("Executing 1st test case  : App login as admin user and set a location of the app");
    boolean isLoginSuccess = login.loginTestForAdmin();
    Assert.assertTrue(isLoginSuccess, "Login Failed ");
    ExtentTestCase.getTest().info("Executed 1st test case");
  }

  /**
   * Verify Location screen : Get list of locations.
   */
  @Test(groups = { "smoke", "regression" }, priority = 2, enabled = true)
  public void verifyLocationScreen() throws InterruptedException {
    LOGGER.info("Executing 2nd test case : Login Screen test.");
    ExtentTestCase.setTest(ExtentTestCase.getExtent().createTest("Location screen : Verify List of locations with api",
        "Test the location screen module."));
    ExtentTestCase.getTest().info("Executing 2nd test case :Verify Location List with Api");
    locationScreen.listOfLocation();
    ExtentTestCase.getTest().info("Executed 2nd test case");
  }
  
  /**
   * Verify Login Functionality for End User and check in process.
   */
  @Test(groups = { "smoke", "regression" }, priority = 3, enabled = true)
  public void VerifyAppLoginForEndUser() throws InterruptedException {
    LOGGER.info("Executing 3rd test case : Login Screen test.");
    ExtentTestCase.setTest(ExtentTestCase.getExtent().createTest("Application Login for End User verify check in process",
        "Test the application login module for end user"));
    ExtentTestCase.getTest().info("Executing 3rd test case : App login with end user ");
    boolean isLoginSuccess = login.loginTestForEndUser();
    Assert.assertTrue(isLoginSuccess, "Login Failed ");
  }
  
  /**
   * Verify Dashboard screen : Check in and cancel button functionality for End User.
   */
  @Test(groups = { "smoke", "regression" }, priority = 4, enabled = true)
  public void verifyCancelAndCheckinButtonFunctionality() throws InterruptedException {
    LOGGER.info("Executing 4th test case :Check in and cancel button functionality for End User.");
    ExtentTestCase.setTest(ExtentTestCase.getExtent().createTest("Dashboard Screen : Verify Check in and cancel button functionality ",
        " Verify Dashboard module."));
    ExtentTestCase.getTest().info("Executing 4th test case : Verify Check in and cancel button functionality for End User.");
    dashboard.verifyCancelAndCheckinButtonFuncationality(); 
    ExtentTestCase.getTest().info("Executed 4th test case");
  }
  

  /**
   * Executes after every test.
   * 
   * @param result
   * 
   * @throws IOException
   * @throws InterruptedException
   */
  @AfterMethod(groups = { "smoke", "regression" })
  public void getResult(ITestResult result) throws IOException, InterruptedException {
    LOGGER.info("In after method.");
    String screenShotDestination = null;
    if (result.getStatus() == ITestResult.FAILURE) {
      LOGGER.info("Test Failed.");
      ExtentTestCase.getTest().fail(Status.FAIL + ", Test Case Failed is " + result.getName());
      ExtentTestCase.getTest().fail(Status.FAIL + ", Test Case Failed is " + result.getThrowable());
      screenShotDestination = objCommon.captureScreenShot();
      screenshotUtilObj.onTestFailure(result);
      ExtentTestCase.getTest().log(Status.INFO, "Result Screenshot : "
          + ExtentTestCase.getTest().addScreenCaptureFromPath(screenShotDestination));
      try {
        ExtentTestCase.getTest().log(Status.FAIL,
            "Test Case Failed. Please find screenshot below."
                + " Right click and download the image to open." + "&lt;br&gt;&lt;br&gt;"
                + ExtentTestCase.getTest().addScreenCaptureFromPath(screenShotDestination));
      } catch (IOException e) {
        LOGGER.error("Error while performing actions on file.", e);
      }
      TestSuiteSetup.restartApplication();
      LOGGER.info("Screenshot Displayed in Extent Report");
    } else if (result.getStatus() == ITestResult.SKIP) {
      LOGGER.info("Test skipped.");
      ExtentTestCase.getTest().skip(Status.SKIP + ", '" + result.getName() + "' Test Case Skipped");
    } else if (result.getStatus() == ITestResult.SUCCESS) {
      LOGGER.info("Test Passed.");
      ExtentTestCase.getTest().pass(Status.PASS + ", '" + result.getName() + "' Test Case Passed.");
    }
    ExtentTestCase.getExtent().flush();
  }

  /**
   * Method to release resources.
   * 
   * @throws InterruptedException
   * @throws MalformedURLException
   */
  @AfterTest(groups = { "smoke", "regression" })
  public void tear() throws MalformedURLException, InterruptedException {
    TestSuiteTearDown.teardown();
    LOGGER.info("After test executed");
  }
}
