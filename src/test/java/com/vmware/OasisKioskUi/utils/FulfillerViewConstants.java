/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */
package com.vmware.OasisKioskUi.utils;

/**
 * This class contains the constants of fullfiller view.
 * 
 * @author Mindstix.
 *
 */
public class FulfillerViewConstants {

  public static final String USER_NAME = "WkNhdGFseXN0VGVzdDE=";
  public static final String USER_PASSWORD = "RVRFIVkuZW5hQHlAQTZhNkE3WQ==";
  public static final String USER_NAME_APP = "ZCatalystTest1 Test";
  public static final String USER_NAME_APP_MENU="ZCatalystTest1";
  public static final String OPEN_TICKETS_LABEL = "My Open Tickets";
  public static final String CLOSED_TICKETS_LABEL = "My Closed Tickets";
  public static final String LOCATION_NAME = "IND-Bangalore-Kalyani Vista";
  public static final String PROBLEM_LABEL = "Describe Your Problem";
  public static final String REQUEST_LABEL = "Which type of request is this ?";
  public static final String WATCH_LIST_ID = "My Watch List";
  public static final String CREATE_NEW_TICKET_LABEL = "Create New Ticket";
  public static final String ADD_WATCHER = "Add Watcher";
  public static final String TASK_STATUS_PENDING = "Pending";
  public static final String PENDING_SWITCH_OFF = "Off";
  public static final String PENDING_SWITCH_ON = "On";
  public static final String FINANCE_TICKET = "Finance";
  public static final String TICKET_DESCRIPTION = "Automated New Ticket for Fulfiller.";
  public static final String TASK_CLOSE_COMMENTS = "Automated: Task closing comment.";
}
