/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.OasisKioskUi.screens;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.io.IOUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import com.aventstack.extentreports.Status;
import com.vmware.OasisKioskUi.screens.BaseScreen;
import com.vmware.OasisKioskUi.setup.TestSuiteSetup;
import com.vmware.OasisKioskUi.utils.ExtentTestCase;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import java.util.concurrent.TimeUnit;

/**
 * This class contains all the common methods.
 * 
 * @author Mindstix
 *
 */
public class BaseScreen {
  private static final Logger LOGGER = LoggerFactory.getLogger(BaseScreen.class);

  /**
   * Method for Asserting text of an element.
   * 
   * @param xpath
   *          locator name from properties.
   * @param expectedText
   *          expected text from the element.
   */
  protected void testElementAssert(String xpath, String expectedText) {
    WebDriverWait wait = new WebDriverWait(TestSuiteSetup.driver, 45);
    if (xpath.contains("xpath")) {
      LOGGER.info("Waiting for xpath {}", TestSuiteSetup.selectorProp.getProperty(xpath));
      wait.until(
          ExpectedConditions.visibilityOfElementLocated(By.xpath(TestSuiteSetup.selectorProp.getProperty(xpath))));
      WebElement element = TestSuiteSetup.driver.findElement(By.xpath(TestSuiteSetup.selectorProp.getProperty(xpath)));
      String elementText = element.getText();
      
      LOGGER.info("Expected = {} , Actual = {} ", expectedText, elementText);
      ExtentTestCase.getTest().log(Status.INFO, "Expected = " + expectedText + ", Actual = " + elementText);
      Assert.assertEquals(expectedText, elementText);
      LOGGER.info("Assertion Success");
    } else if (xpath.contains("acc_id")) {
      LOGGER.info("Waiting for Acc ID {}", TestSuiteSetup.selectorProp.getProperty(xpath));
      wait.until(ExpectedConditions
          .visibilityOfElementLocated(MobileBy.AccessibilityId(TestSuiteSetup.selectorProp.getProperty(xpath))));
      WebElement element = TestSuiteSetup.driver
          .findElement(MobileBy.AccessibilityId(TestSuiteSetup.selectorProp.getProperty(xpath)));
      String elementText = element.getText();
      LOGGER.info("Expected = {} , Actual = {} ", expectedText, elementText);
      ExtentTestCase.getTest().log(Status.INFO, "Expected = " + expectedText + ", Actual = " + elementText);
      Assert.assertEquals(expectedText, elementText);
      LOGGER.info("Assertion Success");
    } else {
      LOGGER.info("Waiting for ID {}", TestSuiteSetup.selectorProp.getProperty(xpath));
      wait.until(
          ExpectedConditions.visibilityOfElementLocated(MobileBy.id(TestSuiteSetup.selectorProp.getProperty(xpath))));
      WebElement element = TestSuiteSetup.driver
          .findElement(MobileBy.id(TestSuiteSetup.selectorProp.getProperty(xpath)));
      String elementText = element.getText();
      LOGGER.info("Expected = {} , Actual = {}", expectedText, elementText);
      ExtentTestCase.getTest().log(Status.INFO, "Expected = " + expectedText + ", Actual = " + elementText);
      Assert.assertEquals(expectedText, elementText);
      LOGGER.info("Assertion Success");
    }
  }

  /**
   * Return element after asserting.
   * 
   * @param xpath
   *          locator name from properties.
   * @param expectedText
   *          expected text from the element
   * @return element after asserting.
   */
  protected WebElement getElementAfterAssertEquals(String xpath, String expectedText) {
    WebDriverWait wait = new WebDriverWait(TestSuiteSetup.driver, 45);
    WebElement element;
    if (xpath.contains("xpath")) {
      LOGGER.info("Waiting for xpath {}", TestSuiteSetup.selectorProp.getProperty(xpath));
      wait.until(
          ExpectedConditions.visibilityOfElementLocated(By.xpath(TestSuiteSetup.selectorProp.getProperty(xpath))));
      element = TestSuiteSetup.driver.findElement(By.xpath(TestSuiteSetup.selectorProp.getProperty(xpath)));
      String elementText = element.getAttribute(TestSuiteSetup.selectorProp.getProperty("text"));
      Assert.assertNotNull(element);
      Assert.assertEquals(expectedText, elementText);
    } else if (xpath.contains("acc_id")) {
      LOGGER.info("Waiting for Acc ID {}", TestSuiteSetup.selectorProp.getProperty(xpath));
      wait.until(ExpectedConditions
          .visibilityOfElementLocated(MobileBy.AccessibilityId(TestSuiteSetup.selectorProp.getProperty(xpath))));
      element = TestSuiteSetup.driver
          .findElement(MobileBy.AccessibilityId(TestSuiteSetup.selectorProp.getProperty(xpath)));
      String elementText = element.getAttribute(TestSuiteSetup.selectorProp.getProperty("text"));
      Assert.assertNotNull(element);
      Assert.assertEquals(expectedText, elementText);
    } else {
      LOGGER.info("Waiting for ID {}", TestSuiteSetup.selectorProp.getProperty(xpath));
      wait.until(
          ExpectedConditions.visibilityOfElementLocated(MobileBy.id(TestSuiteSetup.selectorProp.getProperty(xpath))));
      element = TestSuiteSetup.driver.findElement(MobileBy.id(TestSuiteSetup.selectorProp.getProperty(xpath)));
      String elementText = element.getAttribute(TestSuiteSetup.selectorProp.getProperty("text"));
      Assert.assertNotNull(element);
      Assert.assertEquals(expectedText, elementText);
    }
    return element;
  }

  /**
   * Returning element.
   * 
   * @param xpath
   *          locator name from properties.
   * @return element found by the locator.
   */
  protected MobileElement getElement(String xpath) {
    WebDriverWait wait = new WebDriverWait(TestSuiteSetup.driver, 45);
    MobileElement element;
    if (xpath.contains("xpath")) {
      LOGGER.info("Waiting for xpath {}", TestSuiteSetup.selectorProp.getProperty(xpath));
      wait.until(
          ExpectedConditions.visibilityOfElementLocated(By.xpath(TestSuiteSetup.selectorProp.getProperty(xpath))));
      element = TestSuiteSetup.driver.findElement(By.xpath(TestSuiteSetup.selectorProp.getProperty(xpath)));
      Assert.assertNotNull(element);
    } else if (xpath.contains("acc_id")) {
      LOGGER.info("Waiting for Acc Id :{} ", TestSuiteSetup.selectorProp.getProperty(xpath));
      wait.until(ExpectedConditions
          .visibilityOfElementLocated(MobileBy.AccessibilityId(TestSuiteSetup.selectorProp.getProperty(xpath))));
      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
        LOGGER.error("Error while waiting for Acc Id : {} ", xpath, e);
      }
      element = TestSuiteSetup.driver
          .findElement(MobileBy.AccessibilityId(TestSuiteSetup.selectorProp.getProperty(xpath)));
      Assert.assertNotNull(element);
    } else {
      {
        LOGGER.info("Waiting for Id :{} ", TestSuiteSetup.selectorProp.getProperty(xpath));
        wait.until(
            ExpectedConditions.visibilityOfElementLocated(MobileBy.id(TestSuiteSetup.selectorProp.getProperty(xpath))));
        try {
          Thread.sleep(2000);
        } catch (InterruptedException e) {
          LOGGER.error("Error while waiting for ID.", e);
        }
        element = TestSuiteSetup.driver.findElement(MobileBy.id(TestSuiteSetup.selectorProp.getProperty(xpath)));
        Assert.assertNotNull(element);
      }
    }
    return element;
  }

  /**
   * Returns list of mobile elements.
   * 
   * @param xpath
   *          locator name from properties.
   * @return list of mobile elements found by the locator.
   */
  protected List<MobileElement> getElements(String xpath) {
    WebDriverWait wait = new WebDriverWait(TestSuiteSetup.driver, 30);
    List<MobileElement> elements;

    if (xpath.contains("xpath")) {
      LOGGER.info("Waiting for xpath {}", TestSuiteSetup.selectorProp.getProperty(xpath));
      wait.until(
          ExpectedConditions.visibilityOfElementLocated(By.xpath(TestSuiteSetup.selectorProp.getProperty(xpath))));
      elements = TestSuiteSetup.driver.findElements(By.xpath(TestSuiteSetup.selectorProp.getProperty(xpath)));
      Assert.assertNotNull(elements);
    } else if (xpath.contains("acc_id")) {
      LOGGER.info("Waiting for Acc Id {}", TestSuiteSetup.selectorProp.getProperty(xpath));
      wait.until(ExpectedConditions
          .visibilityOfElementLocated(MobileBy.AccessibilityId(TestSuiteSetup.selectorProp.getProperty(xpath))));
      elements = TestSuiteSetup.driver
          .findElements(MobileBy.AccessibilityId(TestSuiteSetup.selectorProp.getProperty(xpath)));
      Assert.assertNotNull(elements);
    } else {
      LOGGER.info("Waiting for Id {}", TestSuiteSetup.selectorProp.getProperty(xpath));
      wait.until(
          ExpectedConditions.visibilityOfElementLocated(MobileBy.id(TestSuiteSetup.selectorProp.getProperty(xpath))));
      elements = TestSuiteSetup.driver.findElements(MobileBy.id(TestSuiteSetup.selectorProp.getProperty(xpath)));
      Assert.assertNotNull(elements);
    }
    return elements;
  }

  /**
   * Wait for visibility of the element.
   * 
   * @param xpath
   * 
   */
  protected void waitUntilVisible(String xpath) {
    if (xpath.contains("xpath")) {
      while (TestSuiteSetup.driver.findElements(By.xpath(TestSuiteSetup.selectorProp.getProperty(xpath))).isEmpty()) {
      }
    } else if (xpath.contains("acc_id")) {
      while (TestSuiteSetup.driver
          .findElements(MobileBy.AccessibilityId(TestSuiteSetup.selectorProp.getProperty(xpath))).isEmpty()) {
      }
    } else {
      while (TestSuiteSetup.driver.findElements(MobileBy.id(TestSuiteSetup.selectorProp.getProperty(xpath)))
          .isEmpty()) {
      }
    }
  }

  /**
   * Wait for a elements visibility for specified time.
   * 
   * @param xpath
   *          locator name from properties.
   */
  protected void waitUntilVisiblityWithTime(String xpath) {
    WebDriverWait wait = new WebDriverWait(TestSuiteSetup.driver, 45);
    if (xpath.contains("xpath")) {
      LOGGER.info("Waiting for xpath : {} ", xpath);
      wait.until(
          ExpectedConditions.visibilityOfElementLocated(By.xpath(TestSuiteSetup.selectorProp.getProperty(xpath))));
    } else if (xpath.contains("acc_id")) {
      LOGGER.info("Waiting for acc_id : {} ", xpath);
      wait.until(ExpectedConditions
          .visibilityOfElementLocated(MobileBy.AccessibilityId(TestSuiteSetup.selectorProp.getProperty(xpath))));
    } else {
      LOGGER.info("Waiting for id : {} ", xpath);
      wait.until(
          ExpectedConditions.visibilityOfElementLocated(MobileBy.id(TestSuiteSetup.selectorProp.getProperty(xpath))));
    }
  }

  /**
   * It returns true if element is present.
   * 
   * @param xpath
   * 
   */
  protected boolean isElementPresent(String xpath) {
    boolean isPresent = false;
    if (xpath.contains("xpath")) {
      isPresent = TestSuiteSetup.driver.findElementsByXPath(TestSuiteSetup.selectorProp.getProperty(xpath)).size() != 0;
      if (isPresent) {
        isPresent = true;
        LOGGER.info("Element Present :{} ", xpath);
      }
    } else if (xpath.contains("id")) {
      isPresent = TestSuiteSetup.driver.findElements(MobileBy.id(TestSuiteSetup.selectorProp.getProperty(xpath)))
          .size() != 0;
      if (isPresent) {
        isPresent = true;
        LOGGER.info("Element Present : {} ", xpath);
      }
    } else {
      isPresent = TestSuiteSetup.driver.findElements(MobileBy.id(TestSuiteSetup.selectorProp.getProperty(xpath)))
          .size() != 0;
      if (isPresent) {
        isPresent = true;
        LOGGER.info("Element Present : {} ", xpath);
      }
    }
    return isPresent;
  }

  /**
   * Wait for a elements invisibility for specified time.
   * 
   * @param xpath
   *          locator name from properties.
   * 
   */
  protected void waitUntilInvisiblityWithTime(String xpath) {
    WebDriverWait wait = new WebDriverWait(TestSuiteSetup.driver, 45);
    if (xpath.contains("xpath")) {
      LOGGER.info("checking invisibility of element by xpath", TestSuiteSetup.selectorProp.getProperty(xpath));
      wait.until(
          ExpectedConditions.invisibilityOfElementLocated(By.xpath(TestSuiteSetup.selectorProp.getProperty(xpath))));
    } else if (xpath.contains("acc_id")) {
      LOGGER.info("checking invisibility of element {} by acc_id", TestSuiteSetup.selectorProp.getProperty(xpath));
      wait.until(ExpectedConditions
          .invisibilityOfElementLocated(MobileBy.AccessibilityId(TestSuiteSetup.selectorProp.getProperty(xpath))));
    } else {
      LOGGER.info("checking invisibility of element {} by id", TestSuiteSetup.selectorProp.getProperty(xpath));
      wait.until(
          ExpectedConditions.invisibilityOfElementLocated(MobileBy.id(TestSuiteSetup.selectorProp.getProperty(xpath))));
    }
  }

  protected void waitForProgressBar(String selector) {
    WebDriverWait wait = new WebDriverWait(TestSuiteSetup.driver, 5);
    if (selector.contains("xpath")) {
      LOGGER.info("checking invisibility of element {} by xpath", TestSuiteSetup.selectorProp.getProperty(selector));
      wait.until(
          ExpectedConditions.invisibilityOfElementLocated(By.xpath(TestSuiteSetup.selectorProp.getProperty(selector))));
    } else if (selector.contains("acc_id")) {
      LOGGER.info("checking invisibility of element {} by acc_id", TestSuiteSetup.selectorProp.getProperty(selector));
      wait.until(ExpectedConditions
          .invisibilityOfElementLocated(MobileBy.AccessibilityId(TestSuiteSetup.selectorProp.getProperty(selector))));
    } else {
      LOGGER.info("checking invisibility of element {} by id", TestSuiteSetup.selectorProp.getProperty(selector));
      wait.until(ExpectedConditions
          .invisibilityOfElementLocated(MobileBy.id(TestSuiteSetup.selectorProp.getProperty(selector))));
    }
  }

  /**
   * Wait for invisibility of the element.
   * 
   * @param xpath
   *          locator name from properties.
   * 
   */
  protected void waitUntilInVisible(String xpath) {
    if (xpath.contains("xpath")) {
      while (!TestSuiteSetup.driver.findElements(By.xpath(TestSuiteSetup.selectorProp.getProperty(xpath))).isEmpty()) {
      }
    } else if (xpath.contains("acc_id")) {
      while (!TestSuiteSetup.driver
          .findElements(MobileBy.AccessibilityId(TestSuiteSetup.selectorProp.getProperty(xpath))).isEmpty()) {
      }
    } else {
      while (!TestSuiteSetup.driver.findElements(MobileBy.id(TestSuiteSetup.selectorProp.getProperty(xpath)))
          .isEmpty()) {
      }
    }
  }

  /**
   * If element contains the expected text.
   * 
   * @param xpath
   *          locator name from properties.
   * @param expectedText
   *          expected text from the element.
   */
  protected Boolean testElementContains(String xpath, String expectedText) {
    Boolean status = false;
    WebDriverWait wait = new WebDriverWait(TestSuiteSetup.driver, 45);
    if (xpath.contains("xpath")) {
      LOGGER.info("In Wait for xpath {}", TestSuiteSetup.selectorProp.getProperty(xpath));
      wait.until(
          ExpectedConditions.visibilityOfElementLocated(By.xpath(TestSuiteSetup.selectorProp.getProperty(xpath))));
      String actualText = TestSuiteSetup.driver.findElement(By.xpath(TestSuiteSetup.selectorProp.getProperty(xpath)))
          .getText();
      Assert.assertNotNull(actualText);
      actualText = actualText.toLowerCase();
      expectedText = expectedText.toLowerCase();
      LOGGER.info("Expected = {} , Actual = {}", expectedText, actualText);
      ExtentTestCase.getTest().log(Status.INFO, "Expected = " + expectedText + ", Actual = " + actualText);
      if (expectedText.length() > actualText.length()) {
        Assert.assertTrue(expectedText.contains(actualText));
        status = true;

      } else {
        Assert.assertTrue(actualText.contains(expectedText));
        status = true;
      }
    } else if (xpath.contains("acc_id")) {
      LOGGER.info("In Wait for Acc ID " + TestSuiteSetup.selectorProp.getProperty(xpath));
      wait.until(ExpectedConditions
          .visibilityOfElementLocated(MobileBy.AccessibilityId(TestSuiteSetup.selectorProp.getProperty(xpath))));
      String actualText = TestSuiteSetup.driver
          .findElement(MobileBy.AccessibilityId(TestSuiteSetup.selectorProp.getProperty(xpath))).getText();
      Assert.assertNotNull(actualText);
      actualText = actualText.toLowerCase();
      expectedText = expectedText.toLowerCase();
      LOGGER.info("Expected = {}, Actual = {}", expectedText, actualText);
      ExtentTestCase.getTest().log(Status.INFO, "Expected = " + expectedText + ", Actual = " + actualText);
      if (expectedText.length() > actualText.length()) {
        Assert.assertTrue(expectedText.contains(actualText));
        status = true;
      } else {
        Assert.assertTrue(actualText.contains(expectedText));
        status = true;
      }
    } else {
      LOGGER.info("In Wait for ID {} ", TestSuiteSetup.selectorProp.getProperty(xpath));
      wait.until(
          ExpectedConditions.visibilityOfElementLocated(MobileBy.id(TestSuiteSetup.selectorProp.getProperty(xpath))));
      String actualText = TestSuiteSetup.driver.findElement(MobileBy.id(TestSuiteSetup.selectorProp.getProperty(xpath)))
          .getText();
      Assert.assertNotNull(actualText);
      actualText = actualText.toLowerCase();
      expectedText = expectedText.toLowerCase();
      LOGGER.info("Expected = {}, Actual = {}", expectedText, actualText);
      ExtentTestCase.getTest().log(Status.INFO, "Expected = " + expectedText + ", Actual = " + actualText);
      if (expectedText.length() > actualText.length()) {
        Assert.assertTrue(expectedText.contains(actualText));
        status = true;
      } else {
        Assert.assertTrue(actualText.contains(expectedText));
        status = true;
      }
    }
    return status;
  }

  /**
   * Scrolls from one element to other.
   */
  protected void scrollFromElementToElement(MobileElement startElement, MobileElement endElement) {
    TouchAction actions = new TouchAction(TestSuiteSetup.driver);
    actions.press(startElement).waitAction(Duration.ofSeconds(2)).moveTo(endElement).release().perform();
    LOGGER.info("Moved to the element");
  }

  /**
   * Perform a swipe down.
   * 
   * @throws InterruptedException
   * 
   */
  protected void swipedowndomain() throws InterruptedException {
    LOGGER.info("Swipe Down Filter-Domain");
    Dimension size = TestSuiteSetup.driver.manage().window().getSize();
    int starty = (int) (size.height * 0.85);
    int endy = (int) (size.height * 0.50);
    int startx = (int) size.width / 2;
    Thread.sleep(1000);
    TouchAction touchAction = new TouchAction(TestSuiteSetup.driver);
    touchAction.press(startx, starty).moveTo(startx, endy).perform();
    Thread.sleep(1000);
  }

  /**
   * Returns the text of the element.
   */
  protected String getElementText(String xpath) {
    WebDriverWait wait = new WebDriverWait(TestSuiteSetup.driver, 45);
    String actualText;
    if (xpath.contains("xpath")) {
      LOGGER.info("Waiting for xpath {}", TestSuiteSetup.selectorProp.getProperty(xpath));
      wait.until(
          ExpectedConditions.visibilityOfElementLocated(By.xpath(TestSuiteSetup.selectorProp.getProperty(xpath))));
      actualText = TestSuiteSetup.driver.findElement(By.xpath(TestSuiteSetup.selectorProp.getProperty(xpath)))
          .getText();
    } else if (xpath.contains("acc_id")) {
      LOGGER.info("Waiting for Acc Id {}", TestSuiteSetup.selectorProp.getProperty(xpath));
      wait.until(ExpectedConditions
          .visibilityOfElementLocated(MobileBy.AccessibilityId(TestSuiteSetup.selectorProp.getProperty(xpath))));
      actualText = TestSuiteSetup.driver
          .findElement(MobileBy.AccessibilityId(TestSuiteSetup.selectorProp.getProperty(xpath))).getText();
    } else {
      LOGGER.info("Waiting for Id {}", TestSuiteSetup.selectorProp.getProperty(xpath));
      wait.until(
          ExpectedConditions.visibilityOfElementLocated(MobileBy.id(TestSuiteSetup.selectorProp.getProperty(xpath))));
      actualText = TestSuiteSetup.driver.findElement(MobileBy.id(TestSuiteSetup.selectorProp.getProperty(xpath)))
          .getText();
    }
    return actualText;
  }

  /**
   * Decodes the passed string using Base64.
   * 
   * @param encodedString
   *          - the string which is to be decoded.
   * @return - decoded string.
   */
  protected String decode(String encodedString) {
    return StringUtils.newStringUtf8(Base64.decodeBase64(encodedString));
  }

  /**
   * Hides the keyboard on the device.
   */
  protected void hideKeyboard() {
    TestSuiteSetup.driver.hideKeyboard();
  }

  /**
   * Extracts and returns integer from passed string.
   * 
   * @param xpath
   *          locator name from properties.
   * @return integer value extracted from the text element
   */
  protected Integer extractNumberFromElementText(String xpath) {
    String text = getElementText(xpath);
    text = text.replaceAll("[^0-9]", "");
    Integer number = Integer.valueOf(text);
    return number;
  }

  /**
   * Performs a swipe up.
   */
  protected void swipeUp() {
    TouchAction actions = new TouchAction(TestSuiteSetup.driver);
    Dimension size = TestSuiteSetup.driver.manage().window().getSize();
    int startx = (int) (size.width / 2);
    int endy = (int) (size.height * 0.40);
    int starty = (int) (size.height * 0.60);
    try {
      if (TestSuiteSetup.targetMobileOS.contains("Android")) {
      } else {
        actions.press(startx, starty).waitAction(Duration.ofSeconds(2)).moveTo(startx, -(size.height - endy)).release()
            .perform();
      }
      Thread.sleep(500);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  /**
   * Swipes screen according to direction and values passed.
   * 
   * @param direction
   *          String value of direction (LEFT, RIGHT, UP, DOWN).
   * @param startVal
   *          approximate multiplier value of start position on the axis of swipe
   *          action.
   * @param endVal
   *          approximate multiplier value of end position on the axis of swipe
   *          action.
   */
  protected void customSwipe(String direction, double startVal, double endVal, double swipeLine) {
    int startx = 0;
    int endx = 0;
    int starty = 0;
    int endy = 0;
    TouchAction actions = new TouchAction(TestSuiteSetup.driver);
    Dimension size = TestSuiteSetup.driver.manage().window().getSize();
    switch (direction.toUpperCase())
    {
      case "LEFT":
        startx = (int) (size.width * startVal);
        endx = (int) (size.width * endVal);
        starty = (int) (size.height * swipeLine);
        endy = (int) (size.height * swipeLine);
        try {
          if (TestSuiteSetup.targetMobileOS.contains("Android")) {
            actions.press(startx, starty).waitAction(Duration.ofSeconds(1)).moveTo(endx, endy).release().perform();
          } else {
            actions.press(startx, starty).waitAction(Duration.ofSeconds(1)).moveTo(startx, -(size.width - endx))
                .release().perform();
          }
          Thread.sleep(2000);
        } catch (InterruptedException e) {
          LOGGER.error("Couldnot perform action. Error occured.", e);
          break;
        }
      case "RIGHT":
        startx = (int) (size.width * startVal);
        endx = (int) (size.width * endVal);
        starty = (int) (size.height * swipeLine);
        endy = (int) (size.height * swipeLine);
        try {
          if (TestSuiteSetup.targetMobileOS.contains("Android")) {
            actions.press(startx, starty).waitAction(Duration.ofSeconds(1)).moveTo(endx, endy).release().perform();
          } else {
            actions.press(startx, starty).waitAction(Duration.ofSeconds(1)).moveTo(startx, (size.width - endx))
                .release().perform();
          }
          Thread.sleep(2000);
        } catch (InterruptedException e) {
          LOGGER.error("Couldnot perform action. Error occured.", e);
          break;
        }
        break;
      case "UP":
        startx = (int) (size.width * swipeLine);
        starty = (int) (size.width * swipeLine);
        starty = (int) (size.height * startVal);
        endy = (int) (size.height * endVal);
        try {
          if (TestSuiteSetup.targetMobileOS.contains("Android")) {
            actions.press(startx, starty).waitAction(Duration.ofSeconds(1)).moveTo(startx, endy).release().perform();
          } else {
            actions.press(startx, starty).waitAction(Duration.ofSeconds(1)).moveTo(startx, -(size.height - endy))
                .release().perform();
          }
          Thread.sleep(2000);
        } catch (InterruptedException e) {
          LOGGER.error("Couldnot perform action. Error occured.", e);
          break;
        }
        break;
      case "DOWN":
        startx = (int) (size.width * swipeLine);
        starty = (int) (size.width * swipeLine);
        starty = (int) (size.height * startVal);
        endy = (int) (size.height * endVal);
        try {
          if (TestSuiteSetup.targetMobileOS.contains("Android")) {
            actions.press(startx, starty).waitAction(Duration.ofSeconds(1)).moveTo(startx, endy).release().perform();
          } else {
            actions.press(startx, starty).waitAction(Duration.ofSeconds(1)).moveTo(startx, (size.height - endy))
                .release().perform();
          }
          Thread.sleep(2000);
        } catch (InterruptedException e) {
          LOGGER.error("Couldnot perform action. Error occured.", e);
          break;
        }
        break;
      default:
        LOGGER.info("Invalid swipe direction provided. "
            + "Please provide any of 'LEFT', 'RIGHT', 'UP', 'DOWN' as direction value.");
        break;
    }
  }

  /**
   * Swipes from a certain point to another point of screen.
   */
  protected void pointToPointSwipe(Point startPoint, Point endPoint) {
    TouchAction swipe = new TouchAction(TestSuiteSetup.driver);
    swipe.press(startPoint.getX(), startPoint.getY()).waitAction(Duration.ofMillis(3000))
        .moveTo(endPoint.getX(), endPoint.getY()).release().perform();
  }

  /**
   * Provide Y coordinate of a locator.
   */
  protected double getRespectiveCenterYPosition(String locator) {
    Dimension size = TestSuiteSetup.driver.manage().window().getSize();
    return ((double) ((MobileElement) getElement(locator)).getCenter().getY()) / (double) size.height;
  }

  /**
   * Method for Capturing Screenshots. And returning the image location
   * 
   * @return screenshot location
   * 
   */
  public String captureScreenShot() {
    File scrFile = ((TakesScreenshot) TestSuiteSetup.driver).getScreenshotAs(OutputType.FILE);
    String encodedBase64 = null;
    FileInputStream fileInputStreamReader = null;
    try {
      fileInputStreamReader = new FileInputStream(scrFile);
      byte[] bytes = new byte[(int) scrFile.length()];
      fileInputStreamReader.read(bytes);
      encodedBase64 = new String(Base64.encodeBase64(bytes));
    } catch (IOException e) {
      LOGGER.error("Exception in performing IO actions.", e);
    } finally {
      IOUtils.closeQuietly(fileInputStreamReader);
    }
    return "data:image/png;base64," + encodedBase64;
  }

  /**
   * Implicit wait
   */
  public void implicitWait() {
    TestSuiteSetup.driver.manage().timeouts().implicitlyWait(35, TimeUnit.SECONDS);
  }

  /**
   * Method to perform pull down to refresh.
   */
  protected void pullDownToRefresh() {
    TouchAction actions = new TouchAction(TestSuiteSetup.driver);
    Dimension screenSize = TestSuiteSetup.driver.manage().window().getSize();
    int startx = (screenSize.width / 2);
    int starty = (int) (screenSize.height * 0.90);
    int endy = (int) (screenSize.height * 0.20);
    try {
      if (TestSuiteSetup.targetMobileOS.contains("Android")) {
        actions.press(startx, endy).waitAction(Duration.ofSeconds(5)).moveTo(startx, starty).release().perform();
      } else {
        // TODO inserted by maqbool at 3:24:39 PM
        // Add method for iOS
      }
      Thread.sleep(500);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  protected void customVerticalSwipe(double startVal, double endVal) {
    TouchAction actions = new TouchAction(TestSuiteSetup.driver);
    Dimension size = TestSuiteSetup.driver.manage().window().getSize();
    int startx = (size.width / 2);
    int endy = (int) (size.height * endVal);
    int starty = (int) (size.height * startVal);
    try {
      if (TestSuiteSetup.targetMobileOS.contains("Android")) {
        actions.press(startx, starty).waitAction(Duration.ofSeconds(2)).moveTo(startx, endy).release().perform();
      } else {
        actions.press(startx, starty).waitAction(Duration.ofSeconds(2)).moveTo(startx, -(size.height - endy)).release()
            .perform();
      }
      Thread.sleep(500);
    } catch (InterruptedException e) {
      LOGGER.error("Couldnot perform action. Error occured.", e);
    }
  }
  
   protected void swipeTicketLeft(MobileElement ticketIdLabel) {
    TouchAction actions = new TouchAction(TestSuiteSetup.driver);
    Dimension size = TestSuiteSetup.driver.manage().window().getSize();
    LOGGER.info("Current Screen width:{}", size.width);
    LOGGER.info("Current Screen height:{}", size.height);
    int startx = (int) (size.width * 0.95);
    int endx = (int) (size.width * 0.05);
    int starty = (int) (size.height * 0.33);
    try {
      if (TestSuiteSetup.targetMobileOS.contains("Android")) {
        actions.press(startx, starty).waitAction(Duration.ofSeconds(2)).moveTo(endx, starty).release().perform();
      } else {
        // TODO inserted by maqbool at 6:34:56 PM
      }
      Thread.sleep(500);
    } catch (InterruptedException e) {
      LOGGER.error("Couldnot perform action. Error occured.", e);
    }
  }
   
   protected void swipeFulfillerTicketLeft(MobileElement ticketIdLabel) {
     TouchAction actions = new TouchAction(TestSuiteSetup.driver);
     Dimension size = TestSuiteSetup.driver.manage().window().getSize();
     LOGGER.info("Current Screen width:{}", size.width);
     LOGGER.info("Current Screen height:{}", size.height);
     int startx = (int) (size.width * 0.95);
     int endx = (int) (size.width * 0.05);
     int starty = (int) (size.height * 0.20);
     try {
       if (TestSuiteSetup.targetMobileOS.contains("Android")) {
         actions.press(startx, starty).waitAction(Duration.ofSeconds(2)).moveTo(endx, starty).release().perform();
       } else {
         // TODO inserted by maqbool at 6:34:56 PM
       }
       Thread.sleep(500);
     } catch (InterruptedException e) {
       LOGGER.error("Couldnot perform action. Error occured.", e);
     }
   }

  /**
   * Clicks on back button.
   */
  public void clickBackButton() {
    TestSuiteSetup.driver.navigate().back();
    LOGGER.info("Clicked on back Button");
  }

  /**
   * Tap an element.
   */
  public void tapElementById(MobileElement tapElement){
    TouchAction tc = new TouchAction(TestSuiteSetup.driver);
    tc.tap(tapElement).perform();
  }
  
  /**
   * Clears the hamburger menu by clicking outside it.
   */
  public void removeHamburgerSlider() {
    LOGGER.info("Clearing the hamburger menu.");
    Dimension size = TestSuiteSetup.driver.manage().window().getSize();
    TouchAction tc = new TouchAction(TestSuiteSetup.driver);
    tc.tap((int) (size.width * 0.95), (int) (size.height * 0.50)).perform();
    LOGGER.info("Cleared the hamburger screen. Back to dashboard.");
  }
  
  /**
   * Clicks on element with matching type of locator.
   * 
   * @param xpath
   *          - String indicating locator name provided in the properties.
   * @param elementName
   *          - String to indicate the element being clicked.
   */
  protected void clickElement(String xpath, String elementName) {
    try {
      WebDriverWait wait = new WebDriverWait(TestSuiteSetup.driver, 60);
      if (xpath.contains("xpath")) {
        LOGGER.info("Clicking on element with xpath : {} ",
            TestSuiteSetup.selectorProp.getProperty(xpath));
        wait.until(ExpectedConditions
            .visibilityOfElementLocated(By.xpath(TestSuiteSetup.selectorProp.getProperty(xpath))));
        TestSuiteSetup.driver.findElement(By.xpath(TestSuiteSetup.selectorProp.getProperty(xpath)))
            .click();
      } else if (xpath.contains("acc_id")) {
        LOGGER.info("Clicking on element with Acc Id : {} ",
            TestSuiteSetup.selectorProp.getProperty(xpath));
        wait.until(ExpectedConditions.visibilityOfElementLocated(
            MobileBy.AccessibilityId(TestSuiteSetup.selectorProp.getProperty(xpath))));
        TestSuiteSetup.driver
            .findElement(MobileBy.AccessibilityId(TestSuiteSetup.selectorProp.getProperty(xpath)))
            .click();
      } else {
        LOGGER.info("Clicking on element with Id : {} ",
            TestSuiteSetup.selectorProp.getProperty(xpath));
        wait.until(ExpectedConditions.visibilityOfElementLocated(
            MobileBy.id(TestSuiteSetup.selectorProp.getProperty(xpath))));
        TestSuiteSetup.driver
            .findElement(MobileBy.id(TestSuiteSetup.selectorProp.getProperty(xpath))).click();
      }
      LOGGER.info("Clicked on {}", elementName);
      ExtentTestCase.getTest().info("Clicked on " + elementName);
    } catch (NoSuchElementException e) {
      LOGGER.info("Error Occured while clicking element:{} ", elementName);
      LOGGER.error("Operation failed!", e);
      throw new NoSuchElementException(
          "Operation Failed while clicking " + elementName + " element");
    } catch (TimeoutException e1) {
      LOGGER.info("Error Occured while clicking element:{} ", elementName);
      LOGGER.error("Operation failed!", e1);
      throw new TimeoutException("Operation Failed while clicking " + elementName + " element");
    } catch (Exception e) {
      LOGGER.info("Error Occured while clicking element:{} ", elementName);
      LOGGER.error("Operation failed!", e);
      throw new RuntimeException("Operation Failed while clicking " + elementName + " element");
    }
  }
  /**
   * Checking element visibility.
   */
  public boolean isElementDisplayed(String elementWithLocator, String webElementName) {
    List<MobileElement> element = null;
    try {
      if (elementWithLocator.contains("id")) {
        LOGGER.info("Waiting for element visibility {}", webElementName);
        LOGGER.info("Element locator is acc ID");
        element = TestSuiteSetup.driver
            .findElements(MobileBy.id(TestSuiteSetup.selectorProp.getProperty(elementWithLocator)));
      } else if (elementWithLocator.contains("xpath")) {
        LOGGER.info("Waiting for element visibility {}", webElementName);
        LOGGER.info("Element locator is xpath");
        element = TestSuiteSetup.driver.findElements(
            MobileBy.xpath(TestSuiteSetup.selectorProp.getProperty(elementWithLocator)));
      } else {
        LOGGER.info("Waiting for element visibility {}", webElementName);
        LOGGER.info("Element locator is id");
        LOGGER.info("Element is visible", webElementName);
        element = TestSuiteSetup.driver.findElements(MobileBy.xpath(elementWithLocator));
      }
      LOGGER.info("Verifying Element");
      if (element.size() != 0) {
        LOGGER.info("{} is Present", webElementName);
        ExtentTestCase.getTest().info(webElementName + " is displayed ");
        return true;
      } else {
        LOGGER.info("{} is NOT Present", webElementName);
        ExtentTestCase.getTest().info(webElementName + " is Not displayed ");
        return false;
      }
    } catch (NoSuchElementException e) {
      LOGGER.info("Error Occured while Searching element:{} ", webElementName);
      LOGGER.error("Operation failed!", e);
      throw new NoSuchElementException(
          "Operation Failed while clicking " + webElementName + " element");
    } catch (TimeoutException e1) {
      LOGGER.info("Error Occured while Searching element:{} ", webElementName);
      LOGGER.error("Operation failed!", e1);
      throw new TimeoutException("Operation Failed while Searching " + webElementName + " element");
    } catch (Exception e) {
      LOGGER.info("Error Occured while Searching element:{} ", webElementName);
      LOGGER.error("Operation failed!", e);
      throw new RuntimeException("Operation Failed while Searching " + webElementName + " element");
    }
  }
  protected void swipeTaskLeft(MobileElement ticketIdLabel) {
    TouchAction actions = new TouchAction(TestSuiteSetup.driver);
    Dimension size = TestSuiteSetup.driver.manage().window().getSize();
    LOGGER.info("Current Screen width:{}", size.width);
    LOGGER.info("Current Screen height:{}", size.height);
    int startx = (int) (size.width * 0.80);
    int endx = (int) (size.width * 0.05);
    int starty = (int) (size.height * 0.50);
    try {
      if (TestSuiteSetup.targetMobileOS.contains("Android")) {
        actions.press(startx, starty).waitAction(Duration.ofSeconds(2)).moveTo(endx, starty).release().perform();
      } else {
        // TODO inserted by maqbool at 6:34:56 PM
      }
      Thread.sleep(500);
    } catch (InterruptedException e) {
      LOGGER.error("Couldnot perform action. Error occured.", e);
    }
  }
}
