/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.OasisKioskUi.utils;

/**
 * This class contains the constants.
 * 
 * @author Mindstix.
 *
 */
public final class Constants {
  public static final String APPIUM_PORT = "4723";

  public static final String DOMAIN_NAME = "vmware.com";
  public static final String DROPDOWN_ID = "userStoreDomain";
  public static final String DROPDOWN_VALUE = "vmware.com";
  public static final String NEXT_ID = "userStoreFormSubmit";
  public static final String ALLOW_PERMISSION = "Allow";
  public static final String DENY_PERMISSION = "Deny";
  public static final String USER_NAME = "Z3VuZHVyZXM=";
  public static final String USER_PASSWORD = "TWluZHN0aXhANzI5Mw==";
  public static final String USER_NAME_ADMIN = "c2hhcm1hYW5raXRh";
  public static final String USER_NAME2 = "ZHZpbmNodXJrYXI=";
  public static final String USER_PASSWORD2 = "TWluZHN0aXhAMTIz";
  public static final String FEEDBACK_MESSAGE = "We would love to hear what you think about this app, provide your";
  public static final String AUTHORIZATION = "Authorization";
  public static final String LT_URL = "http://help-lt.vmware.com/";
  public static final String CHROME_DRIVER="webdriver.chrome.driver";
  public static final String CONTENT_TYPE = "Content-Type";
  public static final int STATUSCODE_200 = 200;
  public static final String REMOTE_USER="X-Remote-User";
  public static final String [] locations = {"PROM C Win10 Dropoff @ Prom Oasis","PROM C Win12","Atlanta Mac Migration","Beijing","Broomfield","ExeC sUPPORT",
      "Executive Support","Hilltop","Kalayani vista","Kalyani Magnum"," Kalyani Vista","Mac Migration/High Sierra Upgrades in HTE 2191",
      "Macau SKO - High Sierra Upgrade","Macau SKO - Oasis Support","Mobile & Cloud Apps @ Hilltop Oasis","Oasis Prod testing-blr","Oasis testing -DTP-blr","OASIS TESTING-Banglaore",
      "Oasis Virtual Kiosk", "OVA test", "Perimeter Center","Prod-smoketest-Oasis","PRODUCTION OASIS TESTING"," Promontory"," Pune Bajaj","Satish Chrome Location",
      "Satish Firefox","Singapore","Staines"};
}
