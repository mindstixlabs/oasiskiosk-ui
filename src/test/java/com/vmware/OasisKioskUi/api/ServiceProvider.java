/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.OasisKioskUi.api;


import static com.jayway.restassured.RestAssured.given;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.jayway.restassured.response.Response;
import com.vmware.OasisKioskUi.setup.ApiSetUp;
import com.vmware.OasisKioskUi.utils.Constants;
import com.vmware.OasisKioskUi.utils.ExtendedUri;
import com.vmware.OasisKioskUi.utils.HeadersParamsConstants;
import com.vmware.OasisKioskUi.utils.RestAssuredUtility;


/**
 * This class contains methods to call APIs required to for test cases.
 * 
 * @author Mindstix
 *
 */
public class ServiceProvider {
  private static final Logger LOGGER = LoggerFactory.getLogger(ServiceProvider.class);
  private Response response;

  /**
   * Method to set base URI and set base path.
   */
  public void setBaseUriAndSetBasePath() {
    RestAssuredUtility.setBaseUri(ApiSetUp.apiEnvProps.getProperty("baseURL"));
    LOGGER.info("Base URI:{}",ApiSetUp.apiEnvProps. getProperty("baseURL"));
    RestAssuredUtility.setBasePath(ApiSetUp.apiEnvProps.getProperty("basePath"));
    LOGGER.info("Base Path:{}",ApiSetUp.apiEnvProps.getProperty("basePath"));
  }
  
  /**
   * Method to get open ticket details for a specific service provider.
   * 
   * @return JSON response containing Open Ticket details for logged in user.
   */
  public JSONObject getHelpDeskDetails() {
    setBaseUriAndSetBasePath();
    response = given().log().all()
        .header(Constants.AUTHORIZATION,
            HeadersParamsConstants.AUTHORIZATION_ALEART + AccessAndRefreshToken.getAccessToken())
        .header(Constants.REMOTE_USER, RestAssuredUtility.decode(Constants.USER_NAME_ADMIN))
        .header(Constants.CONTENT_TYPE, HeadersParamsConstants.ACCEPT).when()
        .get(ExtendedUri.EXTENDED_HELPDESK_URL).then().statusCode(Constants.STATUSCODE_200).extract().response();
    JSONObject jsonResponse = new JSONObject(response.asString());
    LOGGER.info("Response captured Successfully");
    return jsonResponse;
  }
}
