/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.OasisKioskUi.setup;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vmware.OasisKioskUi.api.AccessAndRefreshToken;

/**
 * Setup required before running the test cases.
 * 
 * @author mindstix
 *
 */
public class ApiSetUp {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApiSetUp.class);
    public static final Properties apiEnvProps = new Properties();

    static {
      loadApiProperties();
    }
    
    /**
     * Setup like loading the API properties file, generating auth code, access
     * token and refresh token.
     * 
     * @throws Exception
     *             : File Input output Operation exception
     */
    public static void loadApiProperties() {
        String propertyFilePath = "src" + File.separator + "test" + File.separator + "resources" + File.separator
                + "api-";
        String env = System.getProperty("env.config");
        InputStream input = null;
        try {
            LOGGER.info("Loading 'api-{}-env property' file.", env.toLowerCase());
            input = new FileInputStream(propertyFilePath + env.toLowerCase() + "-env.properties");
            apiEnvProps.load(input);
            LOGGER.info("API Properties File Loaded.");
        } catch (IOException e) {
            LOGGER.error("Error loading property file ", e);
        } finally {
            IOUtils.closeQuietly(input);
        }
        AccessAndRefreshToken.generateAccessTokenAndRefreshToken();
    }
}