/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */
package com.vmware.OasisKioskUi.screens;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.vmware.OasisKioskUi.setup.TestSuiteSetup;
import com.vmware.OasisKioskUi.utils.Constants;
import com.vmware.OasisKioskUi.utils.ExtentTestCase;

import io.appium.java_client.MobileElement;

/**
 * This class contains methods to login with valid credentials and also contains
 * test methods for login screen.
 * 
 * @author Mindstix
 * 
 */
public class Login extends BaseScreen {
  private static final Logger LOGGER = LoggerFactory.getLogger(Login.class);
  public MobileElement locationValue;
  public MobileElement issueType;
  private WebDriver driver;
  private String loginUrl = Constants.LT_URL;
  private String driverPathMac = new File("").getAbsolutePath() + File.separator + File.separator
      + "src" + File.separator + "test" + File.separator + "resources" + File.separator
      + "webdrivers" + File.separator + "mac_chrome_driver" + File.separator + "chromedriver";
  private String driverPathWindows = new File("").getAbsolutePath() + File.separator
      + File.separator + "src" + File.separator + "test" + File.separator + "resources"
      + File.separator + "webdrivers" + File.separator + "mac_chrome_driver" + File.separator
      + "chromedriver";

  /**
   * Login test method: Admin User
   * 
   * @param username
   *          user name for login.
   * @param password
   *          password for login.
   * @return true if logged in successfully.
   * @throws InterruptedException
   */
  public boolean loginTestForAdmin() {
    boolean changeLocationLabel;
    isElementPresent("oasis_logo_acc_id");
    ExtentTestCase.getTest().info("Oasis Logo is present");
    ExtentTestCase.getTest()
        .info(getElementText("Login_screen_scan_message_acc_id") + " label is displayed");
    ExtentTestCase.getTest()
        .info(getElementText("user_name_acc_id") + " textbox label is displayed");
    getElement("user_name_acc_id").sendKeys(decode(Constants.USER_NAME_ADMIN));
    TestSuiteSetup.driver.getKeyboard().sendKeys(Keys.ENTER);
    changeLocationLabel = isElementPresent("Change_location_label_acc_id");
    testElementAssert("Change_location_label_acc_id", "Select location for kiosk");
    ExtentTestCase.getTest().info("Entered Username : user is on change location screen");
    locationValue = getElement("kalyani_vista_acc_id");
    locationValue.click();
    ExtentTestCase.getTest().info("Kalayani vista location is selected");
    getElement("done_acc_id").click();
    return changeLocationLabel;
  }

  /**
   * Login test method: End User
   * 
   * @param username
   *          user name for login.
   * @param password
   *          password for login.
   * @return true if logged in successfully.
   * @throws InterruptedException
   */
  public boolean loginTestForEndUser() {
    boolean changeLocationLabel;
    getElement("user_name_acc_id").sendKeys(decode(Constants.USER_NAME));
    ExtentTestCase.getTest().info("Entered username as a Enduser");
    TestSuiteSetup.driver.getKeyboard().sendKeys(Keys.ENTER);
    changeLocationLabel = isElementPresent("select_reason_label_xpath");
//    testElementAssert("select_reason_label_acc_id", "Select Reason(s) for a check-in");
    ExtentTestCase.getTest().info("'Select Reason for Visit' label is displayed");
    issueType = getElement("hardware_issue_acc_id");
    issueType.click();
    ExtentTestCase.getTest().info("Hardware Issue option is selected for HN+ ticket");
    getElement("Software_issue_acc_id").click();
    ExtentTestCase.getTest().info("Hardware Issue option is selected for HN+ ticket");
    clickElement("check_in_acc_id", "check-in button");
    return changeLocationLabel;
  }

  /**
   * Set up for to launch Chrome browser and
   * 
   */
  public void setup() {
    if (System.getProperty("os.name").contains("Mac OS")) {
      LOGGER.info(Constants.CHROME_DRIVER);
      System.setProperty(Constants.CHROME_DRIVER, driverPathMac);
    } else {
      System.setProperty(Constants.CHROME_DRIVER, driverPathWindows);
    }
    driver = new ChromeDriver();
    driver.get(loginUrl);
    LOGGER.info("Login Url : {} ", loginUrl);
  }
  /**
   * Selection of domain from the dropdown list : vmware.
   */
  public void selectionOfDomain() {
    WebDriverWait wait = new WebDriverWait(driver, 90);
    LOGGER.info("Select the domain");
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(Constants.DROPDOWN_ID)));
    driver.findElement(By.id(Constants.DROPDOWN_ID)).sendKeys(Constants.DROPDOWN_VALUE);
    driver.findElement(By.id(Constants.NEXT_ID)).click();
    LOGGER.info("Selected Domain");
  }

  
  /**
   * Login to the application.
   * 
   */
  public void loginValidCred() {
    // Enter user name
    WebDriverWait wait = new WebDriverWait(driver, 90);
    LOGGER.info("Entering name");
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username")));
    driver.findElement(By.id("username")).sendKeys(decode(Constants.USER_NAME));
    LOGGER.info("Entered user name ");
    // Enter Password
    driver.findElement(By.id("password")).clear();
    driver.findElement(By.id("password"))
        .sendKeys(decode(Constants.USER_PASSWORD));
    LOGGER.info("Entered password");
    // Click on sign in button
    driver.findElement(By.id("signIn")).click();
    LOGGER.info("Clicked on sign in button");
    // Wait till page load and click on grant button
    String currentUrl = driver.getCurrentUrl();
    LOGGER.info("Current URL: {} ", currentUrl);

  }
  }
