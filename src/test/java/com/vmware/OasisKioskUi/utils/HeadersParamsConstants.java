/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.OasisKioskUi.utils;

/**
 * Setting parameters and headers.
 * 
 * @author Mindstix
 *
 */
public final class HeadersParamsConstants {
    public static final String SCOPE = "read";
    public static final String AUTHORIZATION = "Basic aG5fb2FzaXNfbW9iaWxlOnRlc3QxMjM=";
    public static final String AUTHORIZATION_ALEART = "Bearer ";
    public static final String CONTENT_TYPE = "application/x-www-form-urlencoded";
    public static final String ACCEPT = "application/json";
    public static final String REMOTE_USER="X-Remote-User";
}
