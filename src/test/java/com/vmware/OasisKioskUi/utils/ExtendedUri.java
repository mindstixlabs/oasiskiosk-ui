/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.OasisKioskUi.utils;

/**
 * Setting extended URIs.
 * 
 * @author Mindstix
 *
 */
public final class ExtendedUri {
public static final String URL="https://helpnowplus-stage.vmware.com/api/lt/AuthService/oauth/";
public static final String EXTENDEDURL1="/token?grant_type=password&username=svc.np_hn_oasis@vmware.com";
public static final String EXTENDEDURL2="&password=7TT58P27R1QnDiuquop&scope=read&client_id=hn_oasis_mobile";
public static final String EXTENDED_HELPDESK_URL="/lt/oasis/helpdesk";
}