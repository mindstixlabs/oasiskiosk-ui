/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.OasisKioskUi.api;

import static com.jayway.restassured.RestAssured.given;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jayway.restassured.response.Response;
import com.vmware.OasisKioskUi.setup.ApiSetUp;
import com.vmware.OasisKioskUi.utils.Constants;
import com.vmware.OasisKioskUi.utils.ExtendedUri;
import com.vmware.OasisKioskUi.utils.HeadersParamsConstants;
import com.vmware.OasisKioskUi.utils.RestAssuredUtility;


/**
 * This class contains methods to login with valid credentials and get get auth
 * code. This auth code can be used to generate access token and refresh token.
 * 
 * @author Mindstix
 *
 */
public class AccessAndRefreshToken {
  
  private static final Logger LOGGER = LoggerFactory.getLogger(AccessAndRefreshToken.class);
  private static String refreshToken;
  private static String accessToken;
  
  /**
   * Method to get access token.
   * 
   * @return access token
   */
  public static String getAccessToken() {
    return accessToken;
  }

  private static void setAccessToken(final String accessToken) {
    AccessAndRefreshToken.accessToken = accessToken;
  }
  
  /**
   * Method to generate access token and refresh token.
   * 
   */
  public static void generateAccessTokenAndRefreshToken() {
    RestAssuredUtility.setBaseUri(ApiSetUp.apiEnvProps.getProperty("baseURI_Auth"));
    RestAssuredUtility.setBasePath(ApiSetUp.apiEnvProps.getProperty("basePath_Auth"));
    String accessTokenUrl = ExtendedUri.EXTENDEDURL1+ExtendedUri.EXTENDEDURL2; 
    LOGGER.info("acessTokenURL:{}", accessTokenUrl);
    Response res;
    res = given().log().all().header(Constants.AUTHORIZATION, HeadersParamsConstants.AUTHORIZATION)
        .header(Constants.CONTENT_TYPE, HeadersParamsConstants.ACCEPT)
        .when().post(accessTokenUrl).then()
        .statusCode(Constants.STATUSCODE_200).extract().response();
    refreshToken = res.getBody().jsonPath().getString("refresh_token");
    LOGGER.info("Refresh Token captured successfully :{} ", refreshToken);
    AccessAndRefreshToken.setAccessToken(res.getBody().jsonPath().getString("access_token"));
    LOGGER.info("Access Token captured successfully : {} ", accessToken);
  } 
}