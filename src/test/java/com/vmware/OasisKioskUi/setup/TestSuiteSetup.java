/*
 * Copyright (c) 2018 VMware, Inc. All rights reserved.
 *
 */

package com.vmware.OasisKioskUi.setup;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.IOUtils;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeSuite;

import com.vmware.OasisKioskUi.utils.Constants;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

/**
 * Method to do required setups before suite execution.
 * 
 * @author Mindstix
 */
public class TestSuiteSetup {

  private static final Logger LOGGER = LoggerFactory.getLogger(TestSuiteSetup.class);
  public static AppiumDriver<MobileElement> driver;
  public static final Properties selectorProp = new Properties();
  private static final Properties capabilityProp = new Properties();
  private static String appPath;
  private static String appiumIP;
  public static String targetMobileOS;
  private static String noReset;
  private static Boolean isApiSetupDone = false;
  private static String deviceType;

  static {
    loadProperty();
    noReset = capabilityProp.getProperty("noReset");
  }

  /**
   * Method to load properties.
   */
  public static void loadProperty() {
    appiumIP = System.getProperty("env.INSTANCE_IP");
    LOGGER.info("Instance_IP : {}", appiumIP);

    targetMobileOS = System.getProperty("env.OS");
    LOGGER.info("Platform : {}", targetMobileOS);

    appPath = System.getProperty("env.APP_PATH");
    LOGGER.info("AppPath : {}", appPath);

    String suiteName = System.getProperty("env.suiteName");
    LOGGER.info("Suite Name : {}", suiteName);

    // Device types = emulator, simulator, device
    deviceType = System.getProperty("env.deviceType");
    LOGGER.info("Device Type : {}", deviceType);

    InputStream inputLocators = null;
    InputStream inputCapability = null;
    try {
      inputLocators = new FileInputStream("src/test/resources/" + targetMobileOS + ".properties");
      selectorProp.load(inputLocators);
      LOGGER.info("Locator file loaded : src/test/resources/{}.properties", targetMobileOS);
      inputCapability = new FileInputStream(
          "src/test/resources/Appium" + targetMobileOS + "Capabilities" + ".properties");
      capabilityProp.load(inputCapability);
      LOGGER.info("Appium Capabilities File loaded : src/test/resources/Appium{}Capabilities.properties",
          targetMobileOS);
    } catch (IOException ex) {
      LOGGER.error("Error while loading the property file ", ex);
    } finally {
      IOUtils.closeQuietly(inputCapability);
      IOUtils.closeQuietly(inputLocators);
    }

  }

  /**
   * Method for creating a session with the capabilities.
   * 
   * @throws MalformedURLException
   * @throws InterruptedException
   */
  @BeforeSuite
  public static void setUp() throws MalformedURLException, InterruptedException {
    DesiredCapabilities capabilities = new DesiredCapabilities();
    capabilities.setCapability("appium-version", capabilityProp.getProperty("appium_version"));
    capabilities.setCapability("app", appPath);
    capabilities.setCapability("platformVersion", capabilityProp.getProperty("platformVersion"));
    capabilities.setCapability("platformName", targetMobileOS);
    capabilities.setCapability("deviceName", capabilityProp.getProperty("deviceName"));
    capabilities.setCapability("name", capabilityProp.getProperty("app_name"));
    capabilities.setCapability("sendKeyStrategy", capabilityProp.getProperty("sendKeyStrategy"));
    capabilities.setCapability("fullReset", capabilityProp.getProperty("fullReset"));
    capabilities.setCapability("noReset", noReset);
     if (!isApiSetupDone) {
 //    if (suiteName.contains("regression")) {
     LOGGER.info("Setting up prerequisite for service calls.");
     ApiSetUp.loadApiProperties();
     isApiSetupDone=true;
 //    }
     }
    LOGGER.info("Hitting Appium Server at : {}", appiumIP);
    if (targetMobileOS.contains("iOS")) {
      capabilities.setCapability("automationName", capabilityProp.getProperty("automationName"));
      if (targetMobileOS.contains("iOS") && deviceType.contains("device")) {
        capabilities.setCapability("agentPath", capabilityProp.getProperty("agentPath"));
        capabilities.setCapability("bootstrapPath", capabilityProp.getProperty("bootstrapPath"));
        capabilities.setCapability("udid", capabilityProp.getProperty("udid"));
        capabilities.setCapability("xcodeOrgId", capabilityProp.getProperty("xcodeOrgId"));
        capabilities.setCapability("xcodeSigningId", capabilityProp.getProperty("xcodeSigningId"));
        capabilities.setCapability("clearSystemFiles", capabilityProp.getProperty("clearSystemFiles"));
        capabilities.setCapability("wdaStartupRetryInterval", capabilityProp.getProperty("wdaStartupRetryInterval"));
        capabilities.setCapability("useNewWDA", capabilityProp.getProperty("useNewWDA"));
        capabilities.setCapability("waitForQuiescence", capabilityProp.getProperty("waitForQuiescence"));
        capabilities.setCapability("shouldUseSingletonTestManager",
            capabilityProp.getProperty("shouldUseSingletonTestManager"));
      }
      LOGGER.info("Setting driver for iOS");
      driver = new IOSDriver<MobileElement>((new URL("http://" + appiumIP + ":" + Constants.APPIUM_PORT + "/wd/hub")),
          capabilities);
      LOGGER.info("iOS driver loaded.");
    } else {
      LOGGER.info("Setting driver for android");
      capabilities.setCapability("appPackage", capabilityProp.getProperty("appPackage"));
      capabilities.setCapability("appActivity", capabilityProp.getProperty("appActivity"));
      driver = new AndroidDriver<MobileElement>(
          (new URL("http://" + appiumIP + ":" + Constants.APPIUM_PORT + "/wd/hub")), capabilities);
      LOGGER.info("Android driver loaded.");
    }
    driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
  }

  /**
   * Method to restart application.
   * 
   * @throws MalformedURLException
   * @throws InterruptedException
   */
  public static void restartApplication() throws MalformedURLException, InterruptedException {
    driver.quit();
    noReset = "true";
    setUp();
  }
}
